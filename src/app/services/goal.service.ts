import {Injectable} from "@angular/core";
import {environment} from "../../environments/environment";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import { Goal } from "../model/goal";

const apiUrl = environment.apiUrl + '/goal';

@Injectable({
  providedIn: 'root'
})
export class GoalService {

  constructor(private http: HttpClient) {
  }

  list(): Observable<Goal[]> {
    return this.http.get<Goal[]>(`${apiUrl}`);
  }
}