export interface Goal {
  id: number;
  key: string;
  title: string;
}
