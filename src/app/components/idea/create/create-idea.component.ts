import { Component, OnInit } from '@angular/core';
import {AbstractControl, FormBuilder, FormGroup, ValidationErrors, ValidatorFn, Validators} from '@angular/forms';
import {IdeaService} from '../../../services/idea.service';
import {finalize} from 'rxjs/operators';
import {Goal} from '../../../model/goal';
import { GoalService } from 'src/app/services/goal.service';
import { Idea } from 'src/app/model/idea';
import { Router } from '@angular/router';

@Component({
  selector: 'app-create-idea',
  templateUrl: './create-idea.component.html',
  styleUrls: ['./create-idea.component.scss']
})
export class CreateIdeaComponent implements OnInit {

  form: FormGroup;
  goals: Goal[] = [];
  loading: boolean = false;
  loadingError: any | null = null;
  saving: boolean = false;
  savingError: any | null = null;
  submitted: boolean = false;
  success: boolean = false;
  idea: Idea;
  selected = '';

  constructor(private fb: FormBuilder,
              private ideaService: IdeaService,
              private goalService: GoalService,
              private router: Router) {
    this.form = this.fb.group({
      title: [null, Validators.required],
      submitterName: [null],
      goal: [null, Validators.required],
      description: [null, Validators.required],
      imageUrl: [null, this.isValidUrl],
    });
  }

  ngOnInit(): void {
    this.listGoals();
  }

  save() {
    this.submitted = true;
    if (this.form.invalid) {
      return;
    }
    const request = Object.assign({}, this.form.value);
    this.ideaService.create(request)
      .pipe(finalize(() => this.saving = false))
      .subscribe(
        (idea) => {
          this.success = true;
          this.idea = idea;
        },
        error => this.savingError = error
      )
  }

  isValidUrl: ValidatorFn = (control: AbstractControl): ValidationErrors | null => {
    const url = control.value;
    if (!url?.length) {
      return null;
    }
    // Validate image extension
    const imageExtensions = ['png', 'jpg', 'jpg', 'svg', 'gif', 'jpeg'];
    const extension = url.split(".").pop();
    if (imageExtensions.indexOf(extension) == -1) {
      return {invalidUrl: true};
    }
    // Validate url
    try {
      const test = new URL(url);
    } catch (e) {
      return {invalidUrl: true};
    }
    return null;
  }

  listGoals() {
    this.loading = true;
    this.goalService.list().pipe(finalize(() => this.loading = false))
      .subscribe(
        goals => this.goals = goals,
        error => this.loadingError = error
      )
  }

  goToIdea() {
    this.router.navigate(["/idea/" + this.idea.id]);
  }
}
